# Deploying Images in Kubernetes from private Docker repository

### Pre-Requisites
minikube/kubectl installed
Linode account

#### Project Outline
In the below link we will be deploying the below as a microservices application in a Kubernetes cluster for an online shop

Below we have a list of different services attached to the online-shop microservice

https://github.com/GoogleCloudPlatform/microservices-demo/tree/main

![Image1](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image1.png)

We don’t need to understand the actual code to deploy it. We need to understands is the microservices we are deploying, how are they connected and any 3rd Party Services or database we need. Also which microservices need to be accessible from outside the cluster

And in this case it will be the Frontend

![Image2](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image2.png)

Below is a visualisation of the connections needed

![Image3](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image3.png)

In the above architecture we need to configure which environmental variables each microservices expects as well as the ports which each service starts on. 

And since it is one development team that is developing the application we can deploy the microservice in to one namespace

Now lets create a Deployment/Service config for each microservice

![Image4](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image4.png)


#### Getting Started
Let's take this template

![Image5](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image5.png)

Since we have 11 services including redis we should have 11 deployment/services

![Image6](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image6.png)

Using the below link we can get the image version

https://console.cloud.google.com/gcr/images/google-samples/global/microservices-demo/emailservice

![Image7](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image7.png)

![Image8](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image8.png)

And can confirm it is the most u to date version

![Image9](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image9.png)

Now implementing the service as template

![Image10](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image10.png)

When navigating to ‘microservices-demo/src/emailservice/Dockerfile’ in github we can see it starts on port 8080

And the same can be done for service

![Image11](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image11.png)

And also adding the env variable for the container

![Image12](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image12.png)

Now that finalise our email service

Next is the recommendation service

Going on to the link

microservices-demo/src/recommendationservice/Dockerfile

Can see the port starts on 8080

Since the recommendation service also connects to ProductCatalogue service we need to specify where it can connect to it

Semi-finalised recommendationsservice

![Image13](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image13.png)

Next we will do ProductCatalogService

Again going back can see in the docker file

microservices-demo/src/productcatalogservice/Dockerfile

It starts on 3550

Finalised productcatalogue

![Image14](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image14.png)

And now we can input into the recommentationservice

![Image15](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image15.png)

Now doing it for paymentservice

Again in the dockerfile can see it specifies 5051

microservices-demo/src/paymentservice/Dockerfile

Finalised payment service

![Image16](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image16.png)

Next we have the currencyservice

Again in the dockerfile can see it specifies 7000

microservices-demo/src/currencyservice/Dockerfile

![Image17](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image17.png)

Moving on to the shipping service

microservices-demo/src/shippingservice/Dockerfile

Finalised shipping service

![Image18](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image18.png)

Next one is adservice

microservices-demo/src/adservice/Dockerfile

Finalised adservice

![Image19](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image19.png)

Next is cartservice

microservices-demo/src/cartservice/Dockerfile

Since the cart service is connected to redis to save the shopping information

Can configure the cart service

![Image20](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image20.png)

Lets configure the redis-cart

Using official redis docker image, can see it listens on port 6379

https://hub.docker.com/r/redislabs/redisearch

![Image21](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image21.png)

Can now specify it in cart-service

Can specify it for cart-service

Finalised cart-service

![Image22](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image22.png)

Lets configure the redis-cart

Using official redis docker image, can see it listens on port 6379

https://hub.docker.com/r/redislabs/redisearch

![Image23](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image23.png)

Can now specify it in cart-service

Can specify it for cart-service

![Image24](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image24.png)

Finalised cart-service

![Image25](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image25.png)

We need to define a volume so that it can persist data, we can use it for short-term storage

In this case we will use emptyDir

Can then define the emptyDir like the below

![Image26](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image26.png)

And now we need to mount the volume on to the container

Just like the below

![Image27](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image27.png)

Next microservice is checkout

Again in the dockerfile

microservices-demo/src/checkoutservice/Dockerfile it starts on port 5050

Looking at the connection graph, can see it talks to connects to many services

•	Emailservice
•	PaymentService
•	ShippingService
•	CurrencyService
•	ProductCatalogueService
•	CartService
Therefore have to configure the endpoints of each service name + service port for it to connect to

Now if we go into the below directory

microservices-demo/src/checkoutservice/main.go

Can see the definitions to implement

![Image28](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image28.png)

Can then configure all the endpoints using the service port to configure the below

![Image29](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image29.png)

We also need to disable to profiler variable as in the instance it requires it the payment and currency service

![Image30](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image30.png)

Now lets configure the frontend

Just like the previous services it starts on port 8080

microservices-demo/src/frontend/Dockerfile

Now as the frontend speaks to multiple services, we need to configure it as environment variables.

Now using the below from microservices-demo/src/frontend/main.go

![Image31](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image31.png)

Can configure the frontend like the below

![Image32](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image32.png)

Since the clusterIP is an internal service the frontend needs external access

Can then configure the below

![Image33](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image33.png)

Now lets deploy it

First lets spin up our linode cluster

Now running

![Image34](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image34.png)

Also downloading the config file

We also need to secure it

```
chmod 400 online-shop-microservices-kubeconfig-new.yaml
```

Can then configure the below

```
export KUBECONFIG=online-shop-microservices-kubeconfig-new.yaml
```

And test

```
kubectl get node
```

![Image35](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image35.png)

Lets also create a namespace

```
kubectl create ns microservices
kubectl apply -f config.yaml -n microservices
```

And can see all the pods are running

![Image36](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image36.png)

Now accessing it

![Image37](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image37.png)

Should be able to access it on any of the nodes

Using any of the IP’s

```
<Node_IP:Node_Port>
```
![Image38](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image38.png)

IP:Node in the browser

![Image39](https://gitlab.com/FM1995/deploy-microservices-application-2024/-/raw/main/Images/Image39.png)




























